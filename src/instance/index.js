import axios from 'axios';
import headers from '../headers';
import { API_ROOT, TIMEOUT } from '../constants';

/**
 * Create an instance of Axios with configuration
 *
 * @return {Axios} A new instance of Axios
 */
const instance = axios.create({
  baseURL: API_ROOT,
  timeout: TIMEOUT,
  headers
});

export default instance;
