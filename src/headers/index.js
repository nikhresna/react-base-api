import uuid from 'uuid/v4';

const headers = {
  'Content-Type': 'application/json',
  'x-client-type': 'admin',
  'x-client-language': 'en',
  'Idempotency-Key': uuid(),
};

export default headers;
