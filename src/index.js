import instance from './instance';

class BaseApi {
  static getUrl() {
    return ''
  };

  /**
   * Inject X_API_KEY to header
   * Update X_API_KEY header every call
   *
   */
  static initializeInstance() {
    const key = localStorage.getItem('access_token')
    if(key) {
      instance.defaults.headers.common['Authorization'] = `Bearer ${key}`
    }
  }

  /**
   * GET method
   *
   * @param {@Object} obj The axios config
   * @returns {@Promise} instance promise instance
   */
  static get(obj) {
    this.initializeInstance()
    const { url = '', ...config } = obj

    return instance
      .get(`${this.getUrl()}${url}`, { ...config })
  }

  /**
   * POST method
   *
   * @param {@Object} obj The axios config
   * @returns {@Promise} instance promise instance
   */
  static post(obj) {
    this.initializeInstance()
    const { url = '', data, ...config } = obj

    return instance
      .post(`${this.getUrl()}${url}`, data, config )
  }

  /**
   * PATCH method
   *
   * @param {@Object} obj The axios config
   * @returns {@Promise} instance promise instance
   */
  static patch(obj) {
    this.initializeInstance()
    const { url = '', data, ...config } = obj

    return instance
      .patch(`${this.getUrl()}/${url}`, data, { ...config })
  }

  /**
   * DELETE method
   *
   * @param {@String} url The url for deletion resource
   * @returns {@Promise} instance promise instance
   */
  static delete(obj) {
    this.initializeInstance()
    const { url = '', ...config } = obj

    return instance
      .delete(`${this.getUrl()}/${url}`, { ...config } )
  }

  /**
   * Ajax method for form helper
   * to enable dynamic post or patch operation
   * based on edit/add form
   *
   * @param {@String} method The request type post|patch
   * @returns {@Promise} instance promise instance
   */
  static ajax(method, config) {
    if (method === 'post') {
      return this.post(config);
    }

    if (method === 'patch') {
      return this.patch(config);
    }

    throw new Error(`Invalid method: ${method}`);
  }
}

export default BaseApi;
